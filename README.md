# kindred

A library of string similarity functions, based on wrapping existing edit distance metrics

[ ![Codeship Status for naming/kindred](https://codeship.com/projects/c4b1edf0-62fc-0133-535d-62dbb5275a9f/status?branch=master)](https://codeship.com/projects/112718)
[![Codacy Badge](https://api.codacy.com/project/badge/2b7173f81a7a47efa03ec9dbbb530de9)](https://www.codacy.com/app/john-jansen/kindred)

## Contributing to kindred
 
* Check out the latest master to make sure the feature hasn't been implemented or the bug hasn't been fixed yet.
* Check out the issue tracker to make sure someone already hasn't requested it and/or contributed it.
* Fork the project.
* Start a feature/bugfix branch.
* Commit and push until you are happy with your contribution.
* Make sure to add tests for it. This is important so I don't break it in a future version unintentionally.
* Please try not to mess with the Rakefile, version, or history. If you want to have your own version, or is otherwise necessary, that is fine, but please isolate to its own commit so I can cherry-pick around it.

## Copyright

Copyright (c) 2015 Jonathan Kummerfeld. See LICENSE.txt for
further details.

