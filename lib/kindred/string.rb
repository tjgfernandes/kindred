require 'damerau-levenshtein'
require 'text'
require 'set'
require 'pqueue'

STOPWORDS_SMALL = Set.new([
  "a", "an", "and", "as", "at", "for", "in", "is", "of", "on", "or", "that",
  "the", "to"
])

# :nodoc:
class String


  # Copied from https://github.com/threedaymonk/text/blob/master/lib/text/double_metaphone.rb with a couple of modifications
  def slavo_germanic?(str)
    /W|K|CZ|WITZ/ =~ str
  end

  def vowel?(str)
    /^A|E|I|O|U|Y$/ =~ str
  end

  def double_metaphone_lookup(str, pos, length, last)
    case str[pos, 1]
      when /^A|E|I|O|U|Y$/
        if 0 == pos
          return :A, :A, 1
        else
          return nil, nil, 1
        end
      when 'B'
        return :P, :P, ('B' == str[pos + 1, 1] ? 2 : 1)
      when 'Ç'
        return :S, :S, 1
      when 'C'
        if pos > 1 &&
          !vowel?(str[pos - 2, 1]) &&
          'ACH' == str[pos - 1, 3] &&
          str[pos + 2, 1] != 'I' && (
            str[pos + 2, 1] != 'E' ||
            str[pos - 2, 6] =~ /^(B|M)ACHER$/
          ) then
          return :K, :K, 2
        elsif 0 == pos && 'CAESAR' == str[pos, 6]
          return :S, :S, 2
        elsif 'CHIA' == str[pos, 4]
          return :K, :K, 2
        elsif 'CH' == str[pos, 2]
          if pos > 0 && 'CHAE' == str[pos, 4]
            return :K, :X, 2
          elsif 0 == pos && (
              ['HARAC', 'HARIS'].include?(str[pos + 1, 5]) ||
              ['HOR', 'HYM', 'HIA', 'HEM'].include?(str[pos + 1, 3])
            ) && str[0, 5] != 'CHORE' then
            return :K, :K, 2
          elsif ['VAN ','VON '].include?(str[0, 4]) ||
                'SCH' == str[0, 3] ||
                ['ORCHES','ARCHIT','ORCHID'].include?(str[pos - 2, 6]) ||
                ['T','S'].include?(str[pos + 2, 1]) || (
                  ((0 == pos) || ['A','O','U','E'].include?(str[pos - 1, 1])) &&
                  ['L','R','N','M','B','H','F','V','W',' '].include?(str[pos + 2, 1])
                ) then
            return :K, :K, 2
          elsif pos > 0
            return ('MC' == str[0, 2] ? 'K' : 'X'), 'K', 2
          else
            return :X, :X, 2
          end
        elsif 'CZ' == str[pos, 2] && 'WICZ' != str[pos - 2, 4]
          return :S, :X, 2
        elsif 'CIA' == str[pos + 1, 3]
          return :X, :X, 3
        elsif 'CC' == str[pos, 2] && !(1 == pos && 'M' == str[0, 1])
          if /^I|E|H$/ =~ str[pos + 2, 1] && 'HU' != str[pos + 2, 2]
            if (1 == pos && 'A' == str[pos - 1, 1]) ||
              /^UCCE(E|S)$/ =~ str[pos - 1, 5] then
              return :KS, :KS, 3
            else
              return :X, :X, 3
            end
          else
            return :K, :K, 2
          end
        elsif /^C(K|G|Q)$/ =~ str[pos, 2]
          return :K, :K, 2
        elsif /^C(I|E|Y)$/ =~ str[pos, 2]
          return :S, (/^CI(O|E|A)$/ =~ str[pos, 3] ? :X : :S), 2
        else
          if /^ (C|Q|G)$/ =~ str[pos + 1, 2]
            return :K, :K, 3
          else
            return :K, :K, (/^C|K|Q$/ =~ str[pos + 1, 1] && !(['CE','CI'].include?(str[pos + 1, 2])) ? 2 : 1)
          end
        end
      when 'D'
        if 'DG' == str[pos, 2]
          if /^I|E|Y$/ =~ str[pos + 2, 1]
            return :J, :J, 3
          else
            return :TK, :TK, 2
          end
        else
          return :T, :T, (/^D(T|D)$/ =~ str[pos, 2] ? 2 : 1)
        end
      when 'F'
        return :F, :F, ('F' == str[pos + 1, 1] ? 2 : 1)
      when 'G'
        if 'H' == str[pos + 1, 1]
          if pos > 0 && !vowel?(str[pos - 1, 1])
            return :K, :K, 2
          elsif 0 == pos
            if 'I' == str[pos + 2, 1]
              return :J, :J, 2
            else
              return :K, :K, 2
            end
          elsif (pos > 1 && /^B|H|D$/ =~ str[pos - 2, 1]) ||
                (pos > 2 && /^B|H|D$/ =~ str[pos - 3, 1]) ||
                (pos > 3 && /^B|H$/   =~ str[pos - 4, 1])
            return nil, nil, 2
          else
            if (pos > 2 && 'U' == str[pos - 1, 1] && /^C|G|L|R|T$/ =~ str[pos - 3, 1])
              return :F, :F, 2
            elsif pos > 0 && 'I' != str[pos - 1, 1]
              return :K, :K, 2
            else
              return nil, nil, 2
            end
          end
        elsif 'N' == str[pos + 1, 1]
          if 1 == pos && vowel?(str[0, 1]) && !slavo_germanic?(str)
            return :KN, :N, 2
          else
            if 'EY' != str[pos + 2, 2] && 'Y' != str[pos + 1, 1] && !slavo_germanic?(str)
              return :N, :KN, 2
            else
              return :KN, :KN, 2
            end
          end
        elsif 'LI' == str[pos + 1, 2] && !slavo_germanic?(str)
          return :KL, :L, 2
        elsif 0 == pos && ('Y' == str[pos + 1, 1] || /^(E(S|P|B|L|Y|I|R)|I(B|L|N|E))$/ =~ str[pos + 1, 2])
          return :K, :J, 2
        elsif (('ER' == str[pos + 1, 2] || 'Y' == str[pos + 1, 1]) &&
               /^(D|R|M)ANGER$/ !~ str[0, 6] &&
               /^E|I$/ !~ str[pos - 1, 1] &&
               /^(R|O)GY$/ !~ str[pos - 1, 3])
          return :K, :J, 2
        elsif /^E|I|Y$/ =~ str[pos + 1, 1] || /^(A|O)GGI$/ =~ str[pos - 1, 4]
          if (/^V(A|O)N $/ =~ str[0, 4] || 'SCH' == str[0, 3]) || 'ET' == str[pos + 1, 2]
            return :K, :K, 2
          else
            if 'IER ' == str[pos + 1, 4]
              return :J, :J, 2
            else
              return :J, :K, 2
            end
          end
        elsif 'G' == str[pos + 1, 1]
          return :K, :K, 2
        else
          return :K, :K, 1
        end
      when 'H'
        if (0 == pos || vowel?(str[pos - 1, 1])) && vowel?(str[pos + 1, 1])
          return :H, :H, 2
        else
          return nil, nil, 1
        end
      when 'J'
        if 'JOSE' == str[pos, 4] || 'SAN ' == str[0, 4]
          if (0 == pos && ' ' == str[pos + 4, 1]) || 'SAN ' == str[0, 4]
            return :H, :H, 1
          else
            return :J, :H, 1
          end
        else
          current = ('J' == str[pos + 1, 1] ? 2 : 1)

          if 0 == pos && 'JOSE' != str[pos, 4]
            return :J, :A, current
          else
            if vowel?(str[pos - 1, 1]) && !slavo_germanic?(str) && /^A|O$/ =~ str[pos + 1, 1]
              return :J, :H, current
            else
              if last == pos
                return :J, nil, current
              else
                if /^L|T|K|S|N|M|B|Z$/ !~ str[pos + 1, 1] && /^S|K|L$/ !~ str[pos - 1, 1]
                  return :J, :J, current
                else
                  return nil, nil, current
                end
              end
            end
          end
        end
      when 'K'
        return :K, :K, ('K' == str[pos + 1, 1] ? 2 : 1)
      when 'L'
        if 'L' == str[pos + 1, 1]
          if (((length - 3) == pos && /^(ILL(O|A)|ALLE)$/ =~ str[pos - 1, 4]) ||
              ((/^(A|O)S$/ =~ str[last - 1, 2] || /^A|O$/ =~ str[last, 1]) && 'ALLE' == str[pos - 1, 4]))
            return :L, nil, 2
          else
            return :L, :L, 2
          end
        else
          return :L, :L, 1
        end
      when 'M'
        if ('UMB' == str[pos - 1, 3] &&
            ((last - 1) == pos || 'ER' == str[pos + 2, 2])) || 'M' == str[pos + 1, 1]
          return :M, :M, 2
        else
          return :M, :M, 1
        end
      when 'N'
        return :N, :N, ('N' == str[pos + 1, 1] ? 2 : 1)
      when 'Ñ'
        return :N, :N, 1
      when 'P'
        if 'H' == str[pos + 1, 1]
          return :F, :F, 2
        else
          return :P, :P, (/^P|B$/ =~ str[pos + 1, 1] ? 2 : 1)
        end
      when 'Q'
        return :K, :K, ('Q' == str[pos + 1, 1] ? 2 : 1)
      when 'R'
        current = ('R' == str[pos + 1, 1] ? 2 : 1)

        if last == pos && !slavo_germanic?(str) && 'IE' == str[pos - 2, 2] && /^M(E|A)$/ !~ str[pos - 4, 2]
          return nil, :R, current
        else
          return :R, :R, current
        end
      when 'S'
        if /^(I|Y)SL$/ =~ str[pos - 1, 3]
          return nil, nil, 1
        elsif 0 == pos && 'SUGAR' == str[pos, 5]
          return :X, :S, 1
        elsif 'SH' == str[pos, 2]
          if /^H(EIM|OEK|OLM|OLZ)$/ =~ str[pos + 1, 4]
            return :S, :S, 2
          else
            return :X, :X, 2
          end
        elsif /^SI(O|A)$/ =~ str[pos, 3] || 'SIAN' == str[pos, 4]
          return :S, (slavo_germanic?(str) ? :S : :X), 3
        elsif (0 == pos && /^M|N|L|W$/ =~ str[pos + 1, 1]) || 'Z' == str[pos + 1, 1]
          return :S, :X, ('Z' == str[pos + 1, 1] ? 2 : 1)
        elsif 'SC' == str[pos, 2]
          if 'H' == str[pos + 2, 1]
            if /^OO|ER|EN|UY|ED|EM$/ =~ str[pos + 3, 2]
              return (/^E(R|N)$/ =~ str[pos + 3, 2] ? :X : :SK), :SK, 3
            else
              return :X, ((0 == pos && !vowel?(str[3, 1]) && ('W' != str[pos + 3, 1])) ? :S : :X), 3
            end
          elsif /^I|E|Y$/ =~ str[pos + 2, 1]
            return :S, :S, 3
          else
            return :SK, :SK, 3
          end
        else
          return (last == pos && /^(A|O)I$/ =~ str[pos - 2, 2] ? nil : 'S'), 'S', (/^S|Z$/ =~ str[pos + 1, 1] ? 2 : 1)
        end
      when 'T'
        if 'TION' == str[pos, 4]
          return :X, :X, 3
        elsif /^T(IA|CH)$/ =~ str[pos, 3]
          return :X, :X, 3
        elsif 'TH' == str[pos, 2] || 'TTH' == str[pos, 3]
          if /^(O|A)M$/ =~ str[pos + 2, 2] || /^V(A|O)N $/ =~ str[0, 4] || 'SCH' == str[0, 3]
            return :T, :T, 2
          else
            return 0, :T, 2
          end
        else
          return :T, :T, (/^T|D$/ =~ str[pos + 1, 1] ? 2 : 1)
        end
      when 'V'
        return :F, :F, ('V' == str[pos + 1, 1] ? 2 : 1)
      when 'W'
        if 'WR' == str[pos, 2]
          return :R, :R, 2
        end
        pri, sec = nil, nil

        if 0 == pos && (vowel?(str[pos + 1, 1]) || 'WH' == str[pos, 2])
          pri = :A
          sec = vowel?(str[pos + 1, 1]) ? :F : :A
        end

        if (last == pos && vowel?(str[pos - 1, 1])) || 'SCH' == str[0, 3] ||
            /^EWSKI|EWSKY|OWSKI|OWSKY$/ =~ str[pos - 1, 5]
          return pri, "#{sec}F".intern, 1
        elsif /^WI(C|T)Z$/ =~ str[pos, 4]
          return "#{pri}TS".intern, "#{sec}FX".intern, 4
        else
          return pri, sec, 1
        end
      when 'X'
        current = (/^C|X$/ =~ str[pos + 1, 1] ? 2 : 1)

        if !(last == pos && (/^(I|E)AU$/ =~ str[pos - 3, 3] || /^(A|O)U$/ =~ str[pos - 2, 2]))
          return :KS, :KS, current
        else
          return nil, nil, current
        end
      when 'Z'
        if 'H' == str[pos + 1, 1]
          return :J, :J, 2
        else
          current = ('Z' == str[pos + 1, 1] ? 2 : 1)

          if /^Z(O|I|A)$/ =~ str[pos + 1, 2] || (slavo_germanic?(str) && (pos > 0 && 'T' != str[pos - 1, 1]))
            return :S, :TS, current
          else
            return :S, :S, current
          end
        end
      else
        return nil, nil, 1
    end
  end # def double_metaphone_lookup

  def kindred_double_metaphone
    str = self
    primary, secondary, current = [], [], 0
    original, length, last = "#{str}     ".upcase, str.length, str.length - 1
    if /^GN|KN|PN|WR|PS$/ =~ original[0, 2]
      current += 1
    end
    if 'X' == original[0, 1]
      primary << :S
      secondary << :S
      current += 1
    end
    # Modified code - end condition of the loop, and line before return
    while current <= str.length
      a, b, c = double_metaphone_lookup(original, current, length, last)
      primary << a if a
      secondary << b if b
      current += c if c
    end
    primary, secondary = primary.join(""), secondary.join("")
    return primary, (primary == secondary ? nil : secondary)
  end

  # Convert a measure of distance and a max possible distance to a similarity.
  #
  # @param distance [Numerical] the distance calculated
  # @param maximum [Numerical] the maximum possible distance
  # @return [Float] the similarity
  def kindred_distance_to_similarity(distance, maximum)
    return 1.0 if maximum <= 0
    1.0 - (distance / maximum.to_f)
  end

  # Calculates string edit distance.
  # Allowed modifications are, (1) add a character, (2) remove a character, (3)
  # swap two characters.
  #
  # @param other [String] the text to compare this string with.
  # @return [Array] the distance and the maximum possible value.
  def kindred_word_edit_distance(other)
    fail ArgumentError unless other.is_a? String
    max_length = [size, other.size].max
    [DamerauLevenshtein.distance(self, other), max_length]
  end

  # Calculates phonetic edit distance of two strings.
  # Note - only approximate phonetics (e.g. may ignores vowels).
  #
  # @param other [String] the text to compare this string with.
  # @return [Array] the distance and the maximum possible value.
  def kindred_word_phonetic_distance(other)
    fail ArgumentError unless other.is_a? String
    my_phones_list = kindred_double_metaphone
    other_phones_list = other.kindred_double_metaphone
    max_similarity = -1.0
    result = [0, 0] # Initialised to the case of both being empty strings
    my_phones_list.each do |my_phones|
      next if my_phones.nil?
      other_phones_list.each do |other_phones|
        next if other_phones.nil?
        max_length = [my_phones.size, other_phones.size].max
        next if max_length <= 0
        # Calculate edit distance, constrained to standard Levenshtein (not
        # allowed to swap adjacent characters, which the Damerau variant
        # adds).
        distance = DamerauLevenshtein.distance(my_phones, other_phones, 0)
        similarity = 1.0 - (distance / max_length.to_f)
        if max_similarity < similarity
          max_similarity = similarity
          result = [distance, max_length]
        end
      end
    end
    result
  end

  # Calculates the edit distance between lemmas of two strings.
  #
  # @param other [String] the text to compare this string with.
  # @return [Array] the distance and the maximum possible value.
  def kindred_word_lemma_distance(other)
    fail ArgumentError unless other.is_a? String
    my_lemma = Text::PorterStemming.stem(self)
    other_lemma = Text::PorterStemming.stem(other)
    max_length = [size, other.size].max
    return [0, 0] if max_length <= 0
    # No swaps are allowed, only insertion, deletion and substitution.
    cost = DamerauLevenshtein.distance(my_lemma, other_lemma, 0)
    [cost, max_length]
  end

  # Calculates a normalised measure of string edit distance.
  #
  # @param other [String] the text to compare this string with.
  # @param empty_score [Float] the value to return if the query is an empty string.
  # @return [Float] the similarity, in [0.0, 1.0].
  def kindred_word_edit_similarity(other, empty_score = 0.0)
    fail ArgumentError unless other.is_a? String
    return 1.0 if strip == other.strip
    return empty_score if strip == '' or other == ''
    distance, maximum = kindred_word_edit_distance(other)
    kindred_distance_to_similarity(distance, maximum)
  end

  # Calculates a normalised measure of phonetic edit distance.
  #
  # @param other [String] the text to compare this string with.
  # @param empty_score [Float] the value to return if the query is an empty string.
  # @return [Float] the similarity, in [0.0, 1.0].
  def kindred_word_phonetic_similarity(other, empty_score = 0.0)
    fail ArgumentError unless other.is_a? String
    return 1.0 if strip == other.strip
    return empty_score if strip == '' or other == ''
    distance, maximum = kindred_word_phonetic_distance(other)
    kindred_distance_to_similarity(distance, maximum)
  end

  # Calculates the normalised edit distance between lemmas.
  #
  # @param other [String] the text to compare this string with.
  # @param empty_score [Float] the value to return if the query is an empty string.
  # @return [Float] the similarity, in [0.0, 1.0].
  def kindred_word_lemma_similarity(other, empty_score = 0.0)
    fail ArgumentError unless other.is_a? String
    return 1.0 if strip == other.strip
    return empty_score if strip == '' or other == ''
    distance, maximum = kindred_word_lemma_distance(other)
    kindred_distance_to_similarity(distance, maximum)
  end

  # Combines alignment scores to get overall scores.
  #
  # @param alignment [Array[(Float, (Int, Int), Hash)]] the alignment and related scores.
  # @return [Hash] scores for each of the relation types.
  def kindred_alignment_to_scores(other, alignment, extra_word_penalty)
    edit_total = [0, 0]
    phonetic_total = [0, 0]
    lemma_total = [0, 0]
    penalty_count = 0
    alignment.each do |pair|
      if pair[1][1] == -1
        # Word in self unmatched -- receive full penalty
      elsif pair[1][0] == -1
        # Word in other unmatched -- receive only a partial penalty
        other_word = other[pair[1][1]]
        penalty_count += 1 if ! STOPWORDS_SMALL.include?(other_word)
        pair[2]['edit'] = [0, 0]
        pair[2]['phonetic'] = [0, 0]
        pair[2]['lemma'] = [0, 0]
      end
      edit_total[0] += pair[2]['edit'][0]
      edit_total[1] += pair[2]['edit'][1]
      phonetic_total[0] += pair[2]['phonetic'][0]
      phonetic_total[1] += pair[2]['phonetic'][1]
      lemma_total[0] += pair[2]['lemma'][0]
      lemma_total[1] += pair[2]['lemma'][1]
    end
    edit = kindred_distance_to_similarity(edit_total[0], edit_total[1])
    phonetic = kindred_distance_to_similarity(phonetic_total[0], phonetic_total[1])
    lemma = kindred_distance_to_similarity(lemma_total[0], lemma_total[1])
    penalty = penalty_count * extra_word_penalty
    { 'edit' => [edit - penalty, 0.0].max,
      'phonetic' => [phonetic - penalty, 0.0].max,
      'lemma' => [lemma - penalty, 0.0].max }
  end

  # Determine all pairwise scores for words.
  #
  # @param my_words [Array[String]] one array of words.
  # @param other_words [Array[String]] the other array of words.
  # @param empty_score [Float] the value to return if the query is an empty string.
  # @return [Array[(Float, (Int, Int), Hash)]] the pairwise scores.
  def kindred_pairwise_similarities(my_words, other_words)
    pairs = []
    my_words.each_with_index do |my_word, my_pos|
      other_words.each_with_index do |other_word, other_pos|
        # Calculate metrics
        edit = my_word.kindred_word_edit_distance(other_word)
        phonetic = my_word.kindred_word_phonetic_distance(other_word)
        lemma = my_word.kindred_word_lemma_distance(other_word)

        # Convert to distances
        edit_sim = kindred_distance_to_similarity(edit[0], edit[1])
        phonetic_sim = kindred_distance_to_similarity(phonetic[0], phonetic[1])
        lemma_sim = kindred_distance_to_similarity(lemma[0], lemma[1])

        # Calculate an overall score for this pair
        overall = [edit_sim, phonetic_sim, lemma_sim].max

        # Add to the answer list
        pairs.push([overall, [my_pos, other_pos], { 'edit' => edit, 'phonetic' => phonetic, 'lemma' => lemma } ])
      end
    end
    pairs
  end
  
  # Greedily work out alignment.
  #
  # @param my_words [Array[String]] one array of words.
  # @param other_words [Array[String]] the other array of words.
  # @param empty_score [Float] the value to return if the query is an empty string.
  # @return [Array[(Float, (Int, Int), Hash)]] the alignment and related scores.
  def kindred_greedy_alignment_similarity(my_words, other_words, empty_score)
    pairs = kindred_pairwise_similarities(my_words, other_words)
    pq = PQueue.new(pairs)
    my_done = Array.new(my_words.size, false)
    other_done = Array.new(other_words.size, false)
    alignment = []
    while not pq.empty?
      pair = pq.pop
      if not my_done[pair[1][0]] and not other_done[pair[1][1]]
        my_done[pair[1][0]] = true
        other_done[pair[1][1]] = true
        alignment.push(pair)
      end
    end
    my_done.each_with_index do |state, pos|
      if not state
        my_word = my_words[pos]
        scores = {
          'edit' => my_word.kindred_word_edit_distance(''),
          'phonetic' => my_word.kindred_word_phonetic_distance(''),
          'lemma' => my_word.kindred_word_lemma_distance('')
        }
        alignment.push([empty_score, [pos, -1], scores])
      end
    end
    other_done.each_with_index do |state, pos|
      if not state
        other_word = other_words[pos]
        scores = {
          'edit' => ''.kindred_word_edit_distance(other_word),
          'phonetic' => ''.kindred_word_phonetic_distance(other_word),
          'lemma' => ''.kindred_word_lemma_distance(other_word)
        }
        alignment.push([empty_score, [-1, pos], scores])
      end
    end
    alignment
  end

  # Calculates the similarity of two multi-word strings.
  # All one to one alignments can be considered, with the answer being the
  # result with the highest value for any metric.  If no alignment search is
  # requested then the words are aligned sequentially.
  #
  # @param other [String] the text to compare this with.
  # @param search_alignments [Boolean] whether to search for an alignment.
  # @param empty_score [Float] the value to return if the query is an empty string.
  # @return [Hash] the three similarity measures.
  def kindred_string_similarity(other, empty_score = 0.0, extra_word_penalty = 0.15)
    fail ArgumentError unless other.is_a? String
    return { 'edit' => 1.0, 'phonetic' => 1.0, 'lemma' => 1.0 } if strip == other.strip
    return { 'edit' => empty_score, 'phonetic' => empty_score, 'lemma' => empty_score } if strip == '' or other.strip == ''

    my_words = strip.split
    other_words = other.strip.split
    alignment = kindred_greedy_alignment_similarity(my_words, other_words, empty_score)
    kindred_alignment_to_scores(other_words, alignment, extra_word_penalty)
  end

  # Calculates the fraction of words in this string that appear in other.
  # Note that this measure is not symmetrical.
  #
  # @param other [String] the text to compare this with.
  # @param lemma_match [Boolean] whether to match based on lemmas only.
  # @param empty_score [Float] the value to return if the query is an empty string.
  # @return [Float] the fraction.
  def kindred_string_overlap(other, lemma_match = true, empty_score = 0.0)
    fail ArgumentError unless other.is_a? String
    return 1.0 if strip == other.strip

    # Convert to lists of simple word lemmas
    my_words = gsub(/[^\w\s]/, '').split
    return empty_score if my_words.length <= 0
    other_words = other.gsub(/[^\w\s]/, '').split
    if lemma_match
      my_words.map! { |word| Text::PorterStemming.stem(word) }
      other_words.map! { |word| Text::PorterStemming.stem(word) }
    end

    # Calculate ngram overlap
    overlap_scores = []
    max_length = [my_words.length, 3].min
    (1..max_length).each do |length|
      break if overlap_scores.length > 0 && overlap_scores.last == 0
      other_ngrams = Set.new(other_words.each_cons(length))
      value = 1.0 / (max_length * (my_words.length - length + 1))
      overlap = 0
      my_words.each_cons(length) do |ngram|
        overlap += value if other_ngrams.include?(ngram)
      end
      overlap_scores.push(overlap)
    end

    overlap_scores.inject{|sum,score| sum + score}
  end
end
