require_relative '../../lib/kindred/string'

Given(/^My ([^ ]*) words? [ia][sr]e? (.*)$/) do |place, word|
  instance_variable_set("@#{place}_word", word)
end

When(/^I compute a set of similarity scores$/) do
  @scores = @first_word.kindred_string_similarity(@second_word)
end

When(/^I compute how many words in the first set occur in the second$/) do
  @score = @first_word.kindred_string_overlap(@second_word)
end

When(/^I compute ([^ ]*) similarity$/) do |method|
  method = "kindred_word_#{method}_similarity".to_sym
  @score = @first_word.send(method, @second_word)
end

Then(/^I should get a score of (.*)$/) do |target_score|
  expect(@score).to eq(target_score.to_f)
end

Then(/^I should get a ([^ ]*) score of (.*)$/) do |measure, target_score|
  expect(@scores[measure]).to eq(target_score.to_f)
end
